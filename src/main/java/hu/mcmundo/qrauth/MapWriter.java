package hu.mcmundo.qrauth;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.MapInitializeEvent;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import java.awt.image.BufferedImage;
import java.util.HashMap;

public class MapWriter implements Listener {
    private static QRAuth plugin;
    static HashMap<Player,BufferedImage> playerMaps = new HashMap<>();
    private Integer x,z;

    MapWriter(QRAuth plugin) {
        MapWriter.plugin = plugin;
        x = plugin.getServer().getWorlds().get(0).getSpawnLocation().getBlockX() + 10000;
        z = plugin.getServer().getWorlds().get(0).getSpawnLocation().getBlockZ() + 10000;
    }
    private void create(MapView mapView) {
        mapView.setCenterX(x);
        mapView.setCenterZ(z);
        mapView.setScale(MapView.Scale.CLOSEST);
        mapView.getRenderers().clear();
        mapView.addRenderer(new MapRenderer(){

            @Override
            public void render(MapView map, MapCanvas canvas, Player player) {
                BufferedImage img = playerMaps.get(player);
                if (img != null)
                    canvas.drawImage(0,0,img);
            }
        });
    }

    @EventHandler
    public void onMapInitilize(MapInitializeEvent event) {
        MapView mapView = event.getMap();
        create(mapView);

    }


}
