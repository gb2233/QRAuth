package hu.mcmundo.qrauth;

import org.bukkit.plugin.java.JavaPlugin;

public class QRAuth extends JavaPlugin{

	private static QRAuth plugin;

	public void onEnable(){
		plugin = this;

		plugin.saveDefaultConfig();
		plugin.getConfig().options().copyDefaults(true);
		plugin.saveConfig();
		plugin.reloadConfig();
		QRGenerator.reloadGenerator(plugin);
        getServer().getPluginManager().registerEvents(new MapWriter(plugin), plugin);
        plugin.getCommand("qrauth").setExecutor(new QRCommand(plugin));

    }

	public void onDisable(){
	}

    }