package hu.mcmundo.qrauth;

import io.nayuki.fastqrcodegen.QrCode;

import java.awt.*;
import java.awt.image.BufferedImage;

public class QRGenerator {

    private static QrCode.Ecc eccLvl;
    private static Integer scale;
    private static Integer border;

    public QRGenerator(QRAuth plugin) {
    }

    static BufferedImage generate(String qrtext){

        BufferedImage mapImg = QrCode.encodeText(qrtext, eccLvl).toImage(scale, border);
        BufferedImage scaledQR = new BufferedImage(128,128,mapImg.getType());
        Graphics2D g = scaledQR.createGraphics();
        g.drawImage(mapImg,0,0,128,128,null);
        g.dispose();
        return scaledQR;
    }
    static void reloadGenerator(QRAuth plugin){
        eccLvl = QrCode.Ecc.valueOf(plugin.getConfig().getString("ecc","LOW"));
        scale = plugin.getConfig().getInt("scale",2);
        border = plugin.getConfig().getInt("border",1);
    }
}
