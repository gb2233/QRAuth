package hu.mcmundo.qrauth;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class QRCommand implements CommandExecutor {

    private static QRAuth plugin;

    QRCommand(QRAuth plugin) {
        QRCommand.plugin = plugin;

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("qrauth") && !sender.hasPermission("qrauth.generate")) {
            sender.sendMessage(plugin.getConfig().getString("msg.noPerm"));
            return false;
        }
        if (cmd.getName().equalsIgnoreCase("qrauth")) {
            if (args.length == 0) {
                sender.sendMessage("§aQRAuth ver" + plugin.getDescription().getVersion());
                sender.sendMessage("");
                sender.sendMessage("§a/qra gen <input string>");
                sender.sendMessage("§a/qra reload");
                sender.sendMessage("");
            } else if (args[0].equalsIgnoreCase("reload")) {
                plugin.reloadConfig();
                QRGenerator.reloadGenerator(plugin);
                sender.sendMessage(plugin.getConfig().getString("msg.config-reloaded"));
            } else if (args.length > 1 && args[0].equalsIgnoreCase("gen")) {
                if (sender instanceof Player){
                    Player player = (Player)sender;
                    MapWriter.playerMaps.put(player,QRGenerator.generate(args[1]));
                    player.getInventory().clear();
                    player.getInventory().setItemInMainHand(new ItemStack(Material.EMPTY_MAP));
                    player.sendMessage(plugin.getConfig().getString("msg.map-sent"));
                }

            }
        }
        return true;
    }

}
